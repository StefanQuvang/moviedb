from flask import Flask
from imdb import IMDb
from flask_sqlalchemy import SQLAlchemy
import os


# create an instance of the IMDb class
ImdbDB = IMDb()

app = Flask(__name__)
app.config['SECRET_KEY'] = "29ca81704dce1a3d06001d1949a8b095"

DB = os.environ.get('DB')
DBUSER = os.environ.get('DBUSER')
DBPASSWRD = os.environ.get('DBPASSWRD')
DBIP = os.environ.get('DBIP')
DBPORT = os.environ.get('DBPORT')
DBNAME = os.environ.get('DBNAME')

if DB:
    app.config['SQLALCHEMY_DATABASE_URI'] = DB + "://" + DBUSER + ":" + DBPASSWRD + "@" + DBIP + ":" + DBPORT + "/" + DBNAME
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'



db = SQLAlchemy(app)

from MovCo import routes