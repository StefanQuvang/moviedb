from MovCo.Modules.AddMovie import NewMovie
from MovCo.Modules.DBModels import Interaction
from MovCo import db

def ParseFile(file,posts,imdb):
    file = file.decode('utf-8').split(",")
    for id in file:
        if not Interaction.getMovieByID(db,id):
            Movies = NewMovie(imdb,id.encode('utf-8'))
    return Movies

if __name__ == '__main__':
    from imdb import IMDb
    ia = IMDb()
    posts = []
    ParseFile("0370032",posts,ia)