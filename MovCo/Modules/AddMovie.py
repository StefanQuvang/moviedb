from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from MovCo.Modules.ScanLib import CheckMovie
from MovCo.Modules.DBModels import Interaction
from MovCo import db

class FormAddMovie(FlaskForm):
    Select = SubmitField('Select')

class FormFindMovie(FlaskForm):
    Title = StringField('Title')
    Year = IntegerField('Year')
    ImdbID = StringField('ImdbID')
    Search = SubmitField('Search')

class FormSubmitCsvFile(FlaskForm):
    submit = SubmitField('Submit')

def NewMovie(imdb,id):
    print(id)
    Movies = {}
    if id:
        movie = imdb.get_movie(id)
        try:
            Interaction.AddContent(db, Title=movie['title'], Thumbnail=movie['cover url'], Resume=movie['plot'][0],
                                   MediaServer=CheckMovie(movie.movieID), MovieId=movie.movieID)
        except KeyError:
            Interaction.AddContent(db,Title=movie['title'], Thumbnail=movie['cover url'], Resume="", MediaServer=CheckMovie(movie.movieID), MovieId=movie.movieID)

    Interaction.Commit(db)
    return Interaction.GetContent(db)