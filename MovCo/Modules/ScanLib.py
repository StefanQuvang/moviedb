import os

path = "/movies/"
Movies = []


def Update():
    for r, d, f in os.walk(path):
        for file in f:
            if '.mkv' in file:
                start = file.find("[tt") + len("[tt")
                end = file.find("]")
                substring = file[start:end]
                if substring.isnumeric():
                    Movies.append(substring)

def CheckMovie(ID):

    Result = False
    for Movie in Movies:
        if Movie == ID:
            Result = True
            break
        else:
            Result = False
    return Result

if __name__ == '__main__':
    Update()
    print(CheckMovie("0848228"))