from MovCo.Modules.ScanLib import CheckMovie, Update
from MovCo.Modules.DBModels import Interaction
from MovCo import db, ImdbDB


def EditMovie(id,oldid):

    newmovie = ImdbDB.get_movie(id)
    try:
        Interaction.EditContent(db, oldid, newmovie['title'], newmovie['cover url'], newmovie['plot'],CheckMovie(newmovie.movieID), newmovie.movieID)
    except KeyError:
        Interaction.EditContent(db, oldid, newmovie['title'], newmovie['cover url'], "",CheckMovie(newmovie.movieID), newmovie.movieID)

    Interaction.Commit(db)

    posts = Interaction.GetContent(db)

    return posts


def UpdateMovie(posts):
    Update()
    for post in posts:
        EditMovie(post.id,post.id)

