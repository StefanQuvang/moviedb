from MovCo import db


class Movies(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(), unique=False, nullable=False)
    thumbnail = db.Column(db.String())
    resume = db.Column(db.Text)
    mediaServer = db.Column(db.Boolean())
    movieid = db.Column(db.Integer, unique=False)

    def __repr__(self):
        return f"('{self.id}', '{self.title}', '{self.mediaServer}')"

class Interaction:

    db.create_all()

    def GetContent(self):
        DBContent = Movies.query.order_by(Movies.title).all()
        return DBContent

    def AddContent(self, Title, Thumbnail, Resume, MediaServer, MovieId):

        movie = Movies(title=Title, resume=Resume,  mediaServer=MediaServer, id=MovieId, thumbnail=Thumbnail)
        db.session.add(movie)

    def EditContent(self,oldid, Title, Thumbnail, Resume, MediaServer, MovieId):

        movie = Interaction.getMovieByID(self,oldid)

        movie.title = Title
        movie.thumbnail = Thumbnail
        movie.resume = Resume[0]
        movie.mediaServer = MediaServer
        movie.id = MovieId

    def Commit(self):
        db.session.commit()

    def getMovieByID(self,id):
        Movie = Movies.query.get(id)
        return Movie

    def DeleteContent(self,id):
        x = Interaction.getMovieByID(self,id)
        db.session.delete(x)