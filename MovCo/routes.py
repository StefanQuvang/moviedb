from flask import redirect, request, render_template, url_for,send_from_directory
from MovCo import app, ImdbDB, db
from MovCo.Modules.AddMovie import FormAddMovie, FormFindMovie, NewMovie, FormSubmitCsvFile
from MovCo.Modules.EditMovie import EditMovie, UpdateMovie
from MovCo.Modules.UploadCsv import ParseFile
from MovCo.Modules.DBModels import Interaction
import os

posts = Interaction.GetContent(db)
Movie = {}

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'images/favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/')
def index():
    if not posts:
        UpdateMovie(posts)
    return render_template('Index.html', posts=posts)


@app.route('/update')
def update():
    global posts
    UpdateMovie(posts)
    posts = Interaction.GetContent(db)
    return render_template('Index.html', posts=posts)

@app.route('/about', methods=['POST'])
def about():
    return render_template('About.html', posts=posts, title='About')

@app.route("/movie/<id>", methods=['GET','POST'])
def movie(id):
    global Movie
    Movie = Interaction.getMovieByID(db,id)
    return render_template("Movie.html", posts=posts, title=id, Movie=Movie)

@app.route("/deletemovie/<id>")
def deletemovie(id):
    global posts
    Interaction.DeleteContent(db,id)
    Interaction.Commit(db)
    posts = Interaction.GetContent(db)
    return render_template('Index.html', posts=posts)


@app.route('/addmovie',methods=['GET','POST'])
def addmovie():
    global posts
    fAddMovie = FormAddMovie()
    fFindMovie = FormFindMovie()
    fSubmitCsv = FormSubmitCsvFile()
    movies = []

    if fSubmitCsv.submit.data and fSubmitCsv.validate_on_submit():
        # check if the post request has the file part
        file = request.files['file']
        filestring = file.read()
        posts = ParseFile(filestring,posts,ImdbDB)
        return redirect(url_for('index'))

    if fFindMovie.ImdbID.data:
       movies.append(ImdbDB.get_movie(fFindMovie.ImdbID.data))
    elif fFindMovie.Year.data:
       movies = ImdbDB.search_movie(fFindMovie.Title.data + ' (' + str(fFindMovie.Year.data) + ')')
    elif fFindMovie.Title.data:
       movies = ImdbDB.search_movie(fFindMovie.Title.data)

    if fAddMovie.Select.data and fAddMovie.validate_on_submit():
        head, sep, id = request.form["Select"].partition(' - ')
        if not Interaction.getMovieByID(db,id):
            posts = NewMovie(ImdbDB,id.encode('utf-8'))
        return redirect(url_for('index'))
    return render_template('AddMovie.html', posts=posts, title='AddMovie', AddMovie=fAddMovie, FindMovie=fFindMovie, SubmitCsv=fSubmitCsv, movies=movies)

@app.route('/editmovie/<oldid>',methods=['GET','POST'])
def editmovie(oldid):
    global posts
    fAddMovie = FormAddMovie()
    fFindMovie = FormFindMovie()
    movies = []

    if fFindMovie.ImdbID.data:
       movies.append(ImdbDB.get_movie(fFindMovie.ImdbID.data))
    elif fFindMovie.Year.data:
       movies = ImdbDB.search_movie(fFindMovie.Title.data + ' (' + str(fFindMovie.Year.data) + ')')
    elif fFindMovie.Title.data:
       movies = ImdbDB.search_movie(fFindMovie.Title.data)

    if fAddMovie.Select.data and fAddMovie.validate_on_submit():
        head, sep, id = request.form["Select"].partition(' - ')
        posts = EditMovie(id,oldid)
        return redirect(url_for('index'))
    return render_template('EditMovie.html', posts=posts, title='AddMovie', AddMovie=fAddMovie, FindMovie=fFindMovie, movies=movies)
