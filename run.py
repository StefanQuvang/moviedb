from MovCo import  app
import socket

hostname = socket.gethostname()

if __name__ == '__main__':
    app.run(debug=True, host=socket.gethostbyname(hostname))