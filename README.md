# MovCo

MovCo is a selfhosted webtool to create a complete database of a physical movie collection. 
MovCo will scan the digital movie collection and compare with the added movie,
and inform the user which physical movies are on the chosen media server. 
## How to
The first thing to do, is to add new movies, this can be done in two ways:
1. One movie at a time 
1. CSV file container the imdb ID

When a movie is added, thats it. 
A docker instance can be found at docker hub "quvang/movco"

### One Movie
A new movie can be added by typing in the title, year and/or imdb. 
The tool will scan the IMDB and present a list of movies, select one by clicking it, the movie is now added.

### Upload CSV file
Upload a CSV file with id of movies in current collection. The tool will add each imdb id to the database. 

## Requirement
The movies filesname must be in the format :

"title (year) [imdb ID].mkv"

"The Avengers (2012) [tt0848228].mkv"

